package de.fmitconsulting.timerdemo.services;

public interface TimerExpiredHandler {
    public void timerExpired();
}

