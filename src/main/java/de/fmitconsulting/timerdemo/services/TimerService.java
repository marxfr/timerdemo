package de.fmitconsulting.timerdemo.services;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

public class TimerService extends TimerTask {

    Logger log = LoggerFactory.getLogger(TimerService.class);

    public int timerDurationInSeconds;
    final int delayInMillis = 1000;
    Timer timer;
    public Text counter;
    TimerExpiredHandler handler;
    UI currentUI;


    public TimerService(int durationInSeconds, Text c, UI ui , TimerExpiredHandler h) {
        timerDurationInSeconds = durationInSeconds;
        currentUI = ui;
        handler = h;
        timer = new Timer();
        counter = c;
    }

    public void start() {
        timer.scheduleAtFixedRate(this, 0 , delayInMillis);
    }

    public void stop() {
        timer.cancel();
    }

    public void run() {
        if ( timerDurationInSeconds > 0 ) {
            counter.getUI().ifPresent( ui -> ui.access( () -> {
                log.info(String.valueOf(timerDurationInSeconds));
                counter.setText(String.valueOf(timerDurationInSeconds));
                timerDurationInSeconds--;
            }));

        } else {
            timer.cancel();
            log.info("| Stopped");
            handler.timerExpired();
        }
    }

}
