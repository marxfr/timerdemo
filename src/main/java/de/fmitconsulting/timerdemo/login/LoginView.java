package de.fmitconsulting.timerdemo.login;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.VaadinSession;
import de.fmitconsulting.timerdemo.MainView;
import de.fmitconsulting.timerdemo.constants.SessionKeys;

@Push
@Route("login")
public class LoginView extends VerticalLayout  {

    LoginForm loginForm = new LoginForm();

    public LoginView() {

        loginForm.setForgotPasswordButtonVisible(false);

        loginForm.addLoginListener( event -> {
            VaadinSession.getCurrent().setAttribute(SessionKeys.LOGGED_IN_KEY.name(), "true");
            UI.getCurrent().navigate(MainView.class);
        } );
        add(loginForm);
    }

}

