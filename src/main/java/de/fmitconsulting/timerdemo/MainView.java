package de.fmitconsulting.timerdemo;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.VaadinSession;
import de.fmitconsulting.timerdemo.constants.SessionKeys;
import de.fmitconsulting.timerdemo.login.LoginView;
import de.fmitconsulting.timerdemo.services.TimerExpiredHandler;
import de.fmitconsulting.timerdemo.services.TimerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The main view contains a button and a click listener.
 */


@Route("")
@PWA(name = "Timer demo" , shortName = "TimerDemo", enableInstallPrompt = false)
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
@Push
public class MainView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver,  TimerExpiredHandler {

    Logger log = LoggerFactory.getLogger(MainView.class);

    TimerService simpleTimer;
    Text counter = new Text("");
    HorizontalLayout hLayout = new HorizontalLayout();
    UI ui;

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Object loggedIn = VaadinSession.getCurrent().getAttribute(SessionKeys.LOGGED_IN_KEY.name());

        if (loggedIn == null ) {
            log.info("Not logged in");
            event.rerouteTo("login");
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        ui = UI.getCurrent();
        Object obj = ui.getSession().getAttribute("timer") ;
       if (obj == null ) {
            counter = new Text("");
            TimerService timerService = new TimerService(5 , counter , ui , this);
            ui.getSession().setAttribute("timer", timerService);
            simpleTimer = timerService;
            simpleTimer.start();
           hLayout.add(new Text("Remaining time:"));
           hLayout.add(counter);
           add(hLayout);
        } else { simpleTimer = (TimerService) obj;
            counter = new Text(String.valueOf(simpleTimer.timerDurationInSeconds));
            hLayout.add(new Text("Remaining time:"));
            hLayout.add(counter);
            add(hLayout);
            simpleTimer.counter = counter;
        }

    }

    @Override
    public void timerExpired() {
        ui.access( () -> {
            log.info("| Navigated to Login");
            ui.getSession().setAttribute("timer", null);
            ui.getSession().setAttribute(SessionKeys.LOGGED_IN_KEY.name(), null);
            ui.getSession().close();
            ui.navigate(LoginView.class);
        } );
    }

    public MainView() {
        add(new Button("Logout", event -> {
            ui.access(this::timerExpired);
        }));

        add(new Button("30 Seconds", event -> {
            simpleTimer.stop();
            TimerService timerService = new TimerService(30 , counter , ui , this);
            simpleTimer = timerService;
            ui.getSession().setAttribute("timer", timerService);
            timerService.counter.setText("30");
            simpleTimer.start();

        }));

        add(new Button("60 Seconds", event -> {
            simpleTimer.stop();
            TimerService timerService = new TimerService(60 , counter , ui , this);
            simpleTimer = timerService;
            ui.getSession().setAttribute("timer", timerService);
            timerService.counter.setText("60");
            simpleTimer.start();
        }));
    }

}
